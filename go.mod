module bitbucket.org/uwaploe/mp2csv

go 1.21

require (
	github.com/vmihailenco/msgpack v4.0.2+incompatible
	github.com/vmihailenco/msgpack/v5 v5.4.1
)

require (
	github.com/golang/protobuf v1.2.0 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	golang.org/x/net v0.0.0-20180724234803-3673e40ba225 // indirect
	golang.org/x/sync v0.0.0-20181221193216-37e7f081c4d4 // indirect
	google.golang.org/appengine v1.4.0 // indirect
	gopkg.in/check.v1 v0.0.0-20161208181325-20d25e280405 // indirect
)
