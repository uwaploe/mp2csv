package main

import (
	"encoding/json"
	"fmt"
	"io"
	"sort"
	"time"

	msgpack "github.com/vmihailenco/msgpack/v5"
)

type dpRecord struct {
	t    time.Time
	data any
}

var _ msgpack.CustomDecoder = (*dpRecord)(nil)

func (r *dpRecord) DecodeMsgpack(dec *msgpack.Decoder) error {
	val := struct {
		_msgpack    struct{} `msgpack:",as_array"`
		Secs, Usecs int64
		Data        any
	}{}
	if err := dec.Decode(&val); err != nil {
		return err
	}
	r.t = time.Unix(val.Secs, val.Usecs*1000)
	r.data = val.Data

	return nil
}

func (d dpRecord) MarshalJSON() ([]byte, error) {
	val := struct {
		T    string `json:"t"`
		Data any    `json:"data"`
	}{T: d.t.Format(timeFormat), Data: d.data}

	return json.Marshal(val)
}

func (r *dpRecord) VarNames() []string {
	names := make([]string, 0)
	switch val := r.data.(type) {
	case map[string]interface{}:
		for k := range val {
			names = append(names, k)
		}
		sort.Strings(names)
	case string, []byte:
		names = append(names, "value")
	}

	return names
}

func writeVal(w io.Writer, val any) error {
	var err error

	switch x := val.(type) {
	case float32:
		_, err = fmt.Fprintf(w, ",%.3f", x)
	case float64:
		_, err = fmt.Fprintf(w, ",%.5f", x)
	case int, uint, int8, uint8, int16, uint16, int32, uint32, int64, uint64:
		_, err = fmt.Fprintf(w, ",%d", x)
	default:
		_, err = fmt.Fprintf(w, ",\"%v\"", x)
	}

	return err
}

func (r *dpRecord) WriteCsv(w io.Writer, columns []string) error {
	switch val := r.data.(type) {
	case map[string]interface{}:
		for _, col := range columns {
			writeVal(w, val[col]) //nolint:errcheck
		}
	case string, []byte:
		fmt.Fprintf(w, ",\"%v\"", val)
	}

	return nil
}

// Decode a MessagePack encoded dpRecord
func decodeRecord(dec *msgpack.Decoder) (dpRecord, error) {
	var (
		rec dpRecord
	)
	err := dec.Decode(&rec)
	return rec, err
}

func tsToSeconds(t time.Time) float64 {
	return float64(t.UnixNano()) / 1e9
}
