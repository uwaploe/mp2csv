// Mp2csv converts a Deep Profiler Message Pack data file to CSV.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"runtime"
	"strings"

	msgpack "github.com/vmihailenco/msgpack/v5"
)

const timeFormat = "2006-01-02T15:04:05.000000"
const Usage = `Usage: mp2csv [infile [outfile]]

Convert a Deep Profiler Message Pack data file to CSV. If INFILE is not
specified, the program will read from standard input and write to standard
output. If OUTFILE is not specified, the program will read from INFILE and
write to standard output.

`

var Version = "dev"
var BuildDate = "unknown"

var (
	useSecs  bool
	outJson  bool
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprint(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.BoolVar(&useSecs, "unix", false,
		"Output time-stamps as seconds since 1/1/1970 UTC (Unix time)")
	flag.BoolVar(&outJson, "json", outJson,
		"Output as newline-delimited JSON rather than CSV")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	args := parseCmdLine()

	var (
		f    io.ReadCloser
		outf io.WriteCloser
		err  error
	)

	if len(args) > 0 {
		f, err = os.Open(args[0])
		if err != nil {
			log.Fatalf("Cannot open %s", args[0])
		}
		defer f.Close()

		if len(args) > 1 {
			outf, err = os.Create(args[1])
			if err != nil {
				log.Fatalf("Cannot open %s", args[1])
			}
			defer outf.Close()
		} else {
			outf = os.Stdout
		}
	} else {
		flag.Usage()
		os.Exit(1)
	}

	dec := msgpack.NewDecoder(f)
	enc := json.NewEncoder(outf)

	first := true
	var (
		columns []string
	)

	for {
		rec, err := decodeRecord(dec)
		if err != nil {
			if err != io.EOF {
				log.Println(err)
			}
			break
		}

		if outJson {
			err = enc.Encode(rec)
		} else {
			if first {
				columns = rec.VarNames()
				fmt.Fprintf(outf, "time,")
				fmt.Fprintln(outf, strings.Join(columns, ","))
				first = false
			}

			if useSecs {
				fmt.Fprintf(outf, "%.6f", tsToSeconds(rec.t))
			} else {
				fmt.Fprintf(outf, "%s", rec.t.Format(timeFormat))
			}
			rec.WriteCsv(outf, columns)
			_, err = outf.Write([]byte("\n"))
		}

		if err != nil {
			log.Fatalf("write failed: %v", err)
		}
	}
}

//  LocalWords:  outfile unix utc
