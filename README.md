# Deep Profiler Data Dumper

`Mp2csv` is a command-line application to dump the contents of a Deep Profiler [Message Pack](https://msgpack.org/index.html) format data file in CSV format.

## Installation

Download the appropriate compress-tar archive for your OS from the Downloads section of this respository, unpack and copy the application binary to somewhere in your path.

## Usage

The `--help` option provides a summary:

``` shellsession
$ mp2csv --help
Usage: mp2csv [infile [outfile]]

Convert a Deep Profiler Message Pack data file to CSV. If INFILE is not
specified, the program will read from standard input and write to standard
output. If OUTFILE is not specified, the program will read from INFILE and
write to standard output.
  -unix
    	Output time-stamps as seconds since 1/1/1970 UTC (Unix time)
  -utc
    	Output time-stamps in UTC
```

The program will read from a specified file or from standard input. Timestamps are output in your computer's localtime by default but this can be changed using the `--utc` option.

## Examples

``` shellsession
$ mp2csv mmp_1_20190703T063225_486.mpk | head
time,current,mode,pnum,pressure,vbatt
2019-07-02T23:32:36,0,down,486,98.87000,12.00000
2019-07-02T23:32:41,152,down,486,98.94000,12.00000
2019-07-02T23:32:46,326,down,486,99.10000,12.00000
2019-07-02T23:32:51,430,down,486,99.73000,12.00000
2019-07-02T23:32:56,362,down,486,100.72000,12.00000
2019-07-02T23:33:01,238,down,486,101.99000,12.00000
2019-07-02T23:33:05,384,down,486,102.86000,12.00000
2019-07-02T23:33:11,383,down,486,104.36000,12.00000
2019-07-02T23:33:16,338,down,486,105.81000,12.00000
```

``` shellsession
$ mp2csv --utc mmp_1_20190703T063225_486.mpk | head
time,current,mode,pnum,pressure,vbatt
2019-07-03T06:32:36,0,down,486,98.87000,12.00000
2019-07-03T06:32:41,152,down,486,98.94000,12.00000
2019-07-03T06:32:46,326,down,486,99.10000,12.00000
2019-07-03T06:32:51,430,down,486,99.73000,12.00000
2019-07-03T06:32:56,362,down,486,100.72000,12.00000
2019-07-03T06:33:01,238,down,486,101.99000,12.00000
2019-07-03T06:33:05,384,down,486,102.86000,12.00000
2019-07-03T06:33:11,383,down,486,104.36000,12.00000
2019-07-03T06:33:16,338,down,486,105.81000,12.00000
```

``` shellsession
$ mp2csv --utc ctd_1_20190703T063225_486.mpk ctd.csv
$ head ctd.csv
time,condwat,preswat,tempwat
2019-07-03T06:32:35.097324,35.52010,98.99000,8.39640
2019-07-03T06:32:36.031024,35.52010,99.01000,8.39630
2019-07-03T06:32:37.03499,35.52010,99.03000,8.39580
2019-07-03T06:32:38.030113,35.51990,99.02000,8.39540
2019-07-03T06:32:39.033623,35.51970,99.04000,8.39540
2019-07-03T06:32:40.027851,35.51990,99.07000,8.39570
2019-07-03T06:32:41.042351,35.51990,99.07000,8.39530
2019-07-03T06:32:42.026437,35.52010,99.12000,8.39540
2019-07-03T06:32:43.031141,35.51990,99.13000,8.39560
```